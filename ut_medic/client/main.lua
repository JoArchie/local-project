local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local ESX = nil
local playerData = nil
local interactions = {}
local animRunning = false
local isCarry = false
local isSedated = false

function getDistance(coords1, coords2)
  return Vdist2(coords1.x, coords1.y, coords1.z, coords2.x, coords2.y, coords2.z)
end

function loadAnimDict( dict )
  while ( not HasAnimDictLoaded( dict ) ) do
      RequestAnimDict( dict )
      Citizen.Wait( 5 )
  end
end 

Citizen.CreateThread(function()
  while ESX == nil or ESX.PlayerLoaded == false do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end

  playerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	playerData.job = job
end)

AddEventHandler('onResourceStop', function(resource)
	if resource == GetCurrentResourceName() then
    TriggerEvent("ut_medic:releasePatient")
	end
end)

AddEventHandler('playerSpawned', function(spawn)
  isDead = false
  TriggerServerEvent('ut_medic:changeStatus', false)
end)

AddEventHandler('esx:onPlayerDeath', function()
  isDead = true
  TriggerServerEvent('ut_medic:changeStatus', false)
end)

RegisterNetEvent('ut_medic:dragFromMenu')
AddEventHandler('ut_medic:dragFromMenu', function()
  local closestPlayer, distance = ESX.Game.GetClosestPlayer()
  if closestPlayer ~= -1 and distance <= 3.0 then
    TriggerServerEvent('ut_medic:drag', GetPlayerServerId(closestPlayer))
  else
    TriggerEvent('mythic_notify:client:SendAlert', { type = 'error', text = 'No one is close to you to drag' })
  end
end)

RegisterNetEvent('ut_medic:bandageFromMenu')
AddEventHandler('ut_medic:bandageFromMenu', function()
  local closestPlayer, distance = ESX.Game.GetClosestPlayer()
  if closestPlayer ~= -1 and distance <= 3.0 then
    ESX.TriggerServerCallback('ut_medic:getItemAmount', function(quantity)
      if quantity > 0 then
        local closestPlayerPed = GetPlayerPed(closestPlayer)
        local health = GetEntityHealth(closestPlayerPed)

        if health > 0 then
          local playerPed = PlayerPedId()
          TriggerEvent("mythic_progbar:client:progress", {
            name = "bandage",
            duration = 10000,
            label = "Applying Bandage",
            useWhileDead = false,
            canCancel = true,
            controlDisables = {
              disableMovement = true,
              disableCarMovement = true,
              disableMouse = false,
              disableCombat = true,
            },
            animation = {
              animDict = "amb@medic@standing@tendtodead@idle_a",
              anim = "idle_a",
              flags = 49,
              task = nil,
            },
          }, function(status)
            if not status then  
              TriggerServerEvent('removeItem', 'bandage')
              TriggerServerEvent('ut_medic:heal', GetPlayerServerId(closestPlayer), 'bandage')
              TriggerEvent('mythic_notify:client:SendAlert', { type = 'inform', text = 'You have treated the patient' })
            end
          end)
        else
          TriggerEvent('mythic_notify:client:SendAlert', { type = 'error', text = 'Patient needs CPR' })
        end
      else
        TriggerEvent('mythic_notify:client:SendAlert', { type = 'error', text = 'You do not have any bandages' })
      end
    end, 'bandage')
  else
    TriggerEvent('mythic_notify:client:SendAlert', { type = 'error', text = 'No one is close to you to bandage!' })
  end
end)
RegisterNetEvent('ut_medic:medicineFromMenu')
AddEventHandler('ut_medic:medicineFromMenu', function()
  local closestPlayer, distance = ESX.Game.GetClosestPlayer()
  if closestPlayer ~= -1 and distance <= 3.0 then
    ESX.TriggerServerCallback('ut_medic:getItemAmount', function(quantity)
      if quantity > 0 then
        local closestPlayerPed = GetPlayerPed(closestPlayer)
        local health = GetEntityHealth(closestPlayerPed)
        local playerPed = PlayerPedId()

          TriggerEvent("mythic_progbar:client:progress", {
            name = "medicine",
            duration = 10000,
            label = "Treating Patient",
            useWhileDead = false,
            canCancel = true,
            controlDisables = {
              disableMovement = true,
              disableCarMovement = true,
              disableMouse = false,
              disableCombat = true,
            },
            animation = {
              animDict = "amb@medic@standing@tendtodead@idle_a",
              anim = "idle_a",
              flags = 49,
              task = nil,
            },
          }, function(status)
            if not status then  
              TriggerServerEvent('removeItem', 'medicine')
              TriggerServerEvent('ut_medic:heal', GetPlayerServerId(closestPlayer), 'medicine')
              TriggerEvent('mythic_notify:client:SendAlert', { type = 'inform', text = 'You have treated the patient' })
            end
          end)
      else
        TriggerEvent('mythic_notify:client:SendAlert', { type = 'error', text = 'You do not have any medicine to treat the patient.' })
      end
    end, 'medicine')
  else
    TriggerEvent('mythic_notify:client:SendAlert', { type = 'error', text = 'No one is close to you to treat!' })
  end
end)

RegisterNetEvent('ut_medic:heal')
AddEventHandler('ut_medic:heal', function(healType)
	local playerPed = PlayerPedId()
	local maxHealth = GetEntityMaxHealth(playerPed)

	if healType == 'bandage' then
		local health = GetEntityHealth(playerPed)
    local newHealth = math.min(maxHealth, math.floor(health + maxHealth / 4))
    TriggerEvent('mythic_notify:client:SendAlert', { type = 'error', text = 'Bandage is now applied!' })
		SetEntityHealth(playerPed, newHealth)
	elseif healType == 'medicine' then
		TriggerEvent('ut_items:medicine', playerPed)
	end

end)


RegisterNetEvent('ut_medic:drag')
AddEventHandler('ut_medic:drag', function(target)
	local playerPed = PlayerPedId()
	local targetPed = GetPlayerPed(GetPlayerFromServerId(target))

	if isCarry == false then
		animRunning = true
		AttachEntityToEntity(playerPed, targetPed, 9816, 0.015, 0.38, 0.11, 0.9, 0.30, 90.0, false, false, false, false, 2, false)
		isCarry = true
	else
		animRunning = false
		DetachEntity(GetPlayerPed(-1), true, false)
		ClearPedTasksImmediately(targetPed)
		ClearPedTasksImmediately(playerPed)

		isCarry = false
	end

	-- if IsPedDeadOrDying(targetPed, true) then
	-- 	DetachEntity(GetPlayerPed(-1), true, false)
	-- end

end)

RegisterNetEvent('ut_medic:draganim')
AddEventHandler('ut_medic:draganim', function(target)
	local playerPed = GetPlayerPed(-1)
	local targetPed = GetPlayerPed(GetPlayerFromServerId(target))

	RequestAnimDict("amb@code_human_in_car_idles@generic@ps@base")

	while not HasAnimDictLoaded("amb@code_human_in_car_idles@generic@ps@base") do
		Citizen.Wait(10)
	end

	TaskPlayAnim(playerPed, "amb@code_human_in_car_idles@generic@ps@base", "base",8.0, -8, -1, 33, 0, 0, 40, 0)
end)

RegisterNetEvent('ut_medic:draganimems')
AddEventHandler('ut_medic:draganimems', function(target)
	local playerPed = GetPlayerPed(-1)

	RequestAnimDict("anim@heists@box_carry@")

	while not HasAnimDictLoaded("anim@heists@box_carry@") do
		Citizen.Wait(10)
	end

	TaskPlayAnim(playerPed, "anim@heists@box_carry@", "idle", 8.0, 8.0, -1, 50, 0, false, false, false)
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if animRunning then
			DisableControlAction(2, 24, true)
			DisableControlAction(2, 25, true)
			DisableControlAction(2, 257, true)
			DisableControlAction(0, 73, true)
			DisableControlAction(2, 263, true)
			DisableControlAction(0, Keys['TAB'], true)
		end
	end
end)

RegisterNetEvent('ut_medic:SedateFromMenu')
AddEventHandler('ut_medic:SedateFromMenu', function()
  local closestPlayer, distance = ESX.Game.GetClosestPlayer()
  if closestPlayer ~= -1 and distance < 2.0 then
    if playerData.job ~= nil and playerData.job.name == 'ambulance' then
      ESX.TriggerServerCallback('ut_medic:getPlayerState', function(data)
      if data[1] == true then
        TriggerServerEvent('ut_medic:sedatee', GetPlayerServerId(closestPlayer))
        TriggerEvent('mythic_notify:client:SendAlert', { type = 'inform', text = 'You have unsedated the patient' })
      else
        TriggerEvent("mythic_progbar:client:progress", {
          name = "sedate",
          duration = 8000,
          label = "Sedating Patient",
          useWhileDead = false,
          canCancel = true,
          controlDisables = {
            disableMovement = true,
            disableCarMovement = true,
            disableMouse = false,
            disableCombat = true,
          },
          animation = {
            animDict = "amb@medic@standing@tendtodead@idle_a",
            anim = "idle_a",
            flags = 49,
            task = nil,
          },
        }, function(status)
          if not status then  
            TriggerServerEvent('ut_medic:sedatee', GetPlayerServerId(closestPlayer))
            TriggerEvent('mythic_notify:client:SendAlert', { type = 'inform', text = 'You have sedated the patient' })
          end
        end)
        end
      end, GetPlayerServerId(closestPlayer))
    end
  else
    TriggerEvent('mythic_notify:client:SendAlert', { type = 'error', text = 'No one is close to sedate!' })
  end
end)

RegisterNetEvent('ut_medic:Sedated')
AddEventHandler('ut_medic:Sedated', function()
	isSedated    = not isSedated
  local playerPed = PlayerPedId()

  Citizen.CreateThread(function()

    if isSedated then

      TriggerServerEvent('ut_medic:changeStatus', true)

			RequestAnimDict('dead')
			while not HasAnimDictLoaded('dead') do
				Citizen.Wait(100)
      end
      
			TaskPlayAnim(playerPed, 'dead', 'dead_a', 8.0, -8.0, -1, 1, 0, 0, 0, 0)

			SetEnableHandcuffs(playerPed, true)
			DisablePlayerFiring(playerPed, true)
			SetCurrentPedWeapon(playerPed, GetHashKey('WEAPON_UNARMED'), true) -- unarm player
			SetPedCanPlayGestureAnims(playerPed, false)
    else
      getup()
      ClearPedTasks(playerPed)
      ClearPedTasksImmediately(playerPed)
      ClearPedSecondaryTask(playerPed)
      TriggerServerEvent('ut_medic:changeStatus', false)
			ClearPedSecondaryTask(playerPed)
			SetEnableHandcuffs(playerPed, false)
			DisablePlayerFiring(playerPed, false)
			SetPedCanPlayGestureAnims(playerPed, true)
		end
	end)
end)

RegisterNetEvent('ut_medic:releasePatient')
AddEventHandler('ut_medic:releasePatient', function()
  if isSedated then
		local playerPed = PlayerPedId()
    isSedated = false
    ClearPedTasks(playerPed)
    ClearPedTasksImmediately(playerPed)
		ClearPedSecondaryTask(playerPed)
		SetEnableHandcuffs(playerPed, false)
    DisableControlAction(1, 37, false) --Disables INPUT_SELECT_WEAPON (tab) Actions
		DisablePlayerFiring(playerPed, false) -- Disable weapon firing
    SetPedCanPlayGestureAnims(playerPed, true)
    EnableAllControlActions(0)
    TriggerServerEvent('ut_medic:changeStatus', false)
  end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
    local playerPed = PlayerPedId()
    
		if isSedated then
			DisableControlAction(0, 24, true) -- Attack
			DisableControlAction(0, 257, true) -- Attack 2
			DisableControlAction(0, 25, true) -- Aim
			DisableControlAction(0, 263, true) -- Melee Attack 1
      DisableControlAction(0, 263, true) -- Melee Attack 1
      DisableControlAction(0, 263, true) -- Melee Attack 1

			DisableControlAction(0, Keys['R'], true) -- Reload
			DisableControlAction(0, Keys['SPACE'], true) -- Jump
			DisableControlAction(0, Keys['Q'], true) -- Cover
			DisableControlAction(0, Keys['TAB'], true) -- Select Weapon
			DisableControlAction(0, Keys['F'], true) -- Also 'enter'?

			DisableControlAction(0, Keys['F1'], true) -- Disable phone
			DisableControlAction(0, Keys['F2'], true) -- Inventory
			DisableControlAction(0, Keys['F3'], true) -- Animations
			DisableControlAction(0, Keys['F6'], true) -- Job

			DisableControlAction(0, Keys['V'], true) -- Disable changing view
			DisableControlAction(0, Keys['C'], true) -- Disable looking behind
			DisableControlAction(0, Keys['X'], true) -- Disable clearing animation
			DisableControlAction(2, Keys['P'], true) -- Disable pause screen

			DisableControlAction(0, 59, true) -- Disable steering in vehicle
			DisableControlAction(0, 71, true) -- Disable driving forward in vehicle
      DisableControlAction(0, 72, true) -- Disable reversing in vehicle
      
      DisableControlAction(0, 172, true) -- 
      DisableControlAction(0, 175, true) -- 
      DisableControlAction(0, 19, true) -- 
      DisableControlAction(0, 174, true) -- 
      DisableControlAction(0, 173, true) -- 


			DisableControlAction(2, Keys['LEFTCTRL'], true) -- Disable going stealth

			DisableControlAction(0, 47, true)  -- Disable weapon
			DisableControlAction(0, 264, true) -- Disable melee
			DisableControlAction(0, 257, true) -- Disable melee
			DisableControlAction(0, 140, true) -- Disable melee
			DisableControlAction(0, 141, true) -- Disable melee
			DisableControlAction(0, 142, true) -- Disable melee
			DisableControlAction(0, 143, true) -- Disable melee
			DisableControlAction(0, 75, true)  -- Disable exit vehicle
      DisableControlAction(27, 75, true) -- Disable exit vehicle
      
      if (not IsEntityPlayingAnim(PlayerPedId(), "dead", "dead_a", 3) and not isDead) or (IsPedRagdoll(PlayerPedId()) and not isDead) then
	    	  RequestAnimDict('dead')
			  while not HasAnimDictLoaded("dead") do
				  Citizen.Wait(1)
			  end
			  TaskPlayAnim(PlayerPedId(), "dead", "dead_a", 8.0, -8.0, -1, 1, 0, 0, 0, 0)
      end
		else
			Citizen.Wait(1000)
		end
	end
end)


function getup()
  ClearPedSecondaryTask(GetPlayerPed(-1))
  loadAnimDict( "random@crash_rescue@help_victim_up" ) 
  TaskPlayAnim( GetPlayerPed(-1), "random@crash_rescue@help_victim_up", "helping_victim_to_feet_victim", 8.0, 1.0, -1, 49, 0, 0, 0, 0 )
  Citizen.Wait(3000)
  ClearPedSecondaryTask(GetPlayerPed(-1))
end



