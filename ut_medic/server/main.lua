local ESX = nil
local isSedated = {}


Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
  ESX.RegisterServerCallback('ut_medic:getItemAmount', function(source, cb, item)
    local xPlayer = ESX.GetPlayerFromId(source)
    local quantity = xPlayer.getInventoryItem(item).count
  
    cb(quantity)
  end)

  ESX.RegisterServerCallback('ut_medic:getPlayerState', function(source, cb, target)
    cb({isSedated[target]})
  end)

end)


RegisterServerEvent('ut_medic:drag')
AddEventHandler('ut_medic:drag', function(target)
  local _source = source
	local targetPlayer = ESX.GetPlayerFromId(target)

	TriggerClientEvent('ut_medic:draganim', targetPlayer.source, source)
  TriggerClientEvent('ut_medic:drag', target, _source)
  TriggerClientEvent('ut_medic:draganimems', source)
  
end)

RegisterNetEvent('ut_medic:heal')
AddEventHandler('ut_medic:heal', function(target, type)
	local xPlayer = ESX.GetPlayerFromId(source)
		TriggerClientEvent('ut_medic:heal', target, type)
end)

RegisterServerEvent('ut_medic:sedatee')
AddEventHandler('ut_medic:sedatee', function(target)
  TriggerClientEvent('ut_medic:Sedated', target)
end)

RegisterServerEvent('ut_medic:changeStatus')
AddEventHandler('ut_medic:changeStatus', function(state, target)
  local _source = source
  if target == nil then
    if state then
      isSedated[_source] = true
    else
      isSedated[_source] = nil
    end
  else
    if state then
      isSedated[target] = true
    else
      isSedated[target] = nil
    end
  end
end)